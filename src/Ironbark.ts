//@ts-check
/*
  The MDN Web Docs were a huge help in learning JS for me: 
    This link in particular helped to start me with NodeJS...
    https://developer.mozilla.org/en-US/docs/Learn/Server-side/Node_server_without_framework
*/

// Project Includes
// IBBuild::Start

import HTTP = require("http");      // NodeJS HTTP Module
import HTTPS = require("https");    // NodeJS HTTPS Module
import FS = require("fs");          // NodeJS File System Module
import SQLi3 = require("sqlite3");  // NodeJS SQLite3 Module

// IBBuild::End

import IBFoundationImport = require("./IronbarkFoundation");
import IBDataImport = require("./IronbarkData");
import IBModuleLoader = require("./IronbarkModuleLoader");
export const IBFoundation = IBFoundationImport.IBFoundation;
const IBData = IBDataImport.IBData;
IBModuleLoader;

// IBBuild::Start

// IBBuild::RemoveExport
export interface TrunkSettings {
    serverRoot?: string;
    branchServers?: Branch[];
    barkServers?: Bark[];
    defaultRedirect?: Bark;
}

class Configuration {
    // Recursively filter through each folder from the root and find a '.ironbark' file for configuration.
    // Every '.ironbark' becomes a 'root' for the path heierachy.
    constructor (  ) {

    }

}

/*
IronbarkWebServer
|-bin
|-docs
|-modules
|-ironbark-www
 |-Admin
  |-admin1
  |-sysadmin
 |-Users
  |-2020
   |-LENE08
    |-.ironbark
    |-Data
     |-MyWebsite!
  |-2021
  |-2022

*/

// IBBuild::RemoveExport
export class Trunk {

    private httpServer: HTTP.Server;
    private serverPort: number = 80;
    private serverRoot: string;
    public readonly barkServers: Bark[] = [];
    public get port (): number { return this.serverPort };

    constructor ( serverPort: number, serverOptions?: TrunkSettings) {
        this.serverPort = serverPort;
        this.httpServer = HTTP.createServer( (req, res)=>{ this.respondToRequest(req,res); });
        this.serverRoot = "./ironbark-www" ;//|| process.execPath;
    }

    public start (): void {
        this.httpServer.listen( this.serverPort );
        IBFoundation.log(0, "Started Server on PORT:",this.serverPort);
    }

    public stop (): void {
        this.httpServer.close(()=>{
            IBFoundation.log(0, "Closed Server on PORT:",this.serverPort);
        });
    }

    private test(): void {
        //FS.lstatSyn
    }
    

    private respondToRequest ( request: HTTP.IncomingMessage, response: HTTP.ServerResponse, ) {
        
        //IronbarkFoundation.log(2, request.method,">>",request.url);
        IBFoundation.log(2, request.headers);
        switch (request.method) {
            case "HEAD":
                this.respondToHEAD( request, response );
                break;
            case "GET":
                this.respondToGET( request, response );
                break;
            case "POST":
                this.respondToPOST( request, response );
                break;
            default:
                response.writeHead(400, request.method+" request method is not supported.", { "Allow": "GET, POST, HEAD" });
                break;
        }

    }

    private respondToHEAD ( request: HTTP.IncomingMessage, response: HTTP.ServerResponse ) {
        response.writeHead(200);
        response.end();
    }

    private respondToGET ( request: HTTP.IncomingMessage, response: HTTP.ServerResponse ) {

        var filePath = "."+request.url;
        var filePath = this.serverRoot+request.url;
        if (filePath[filePath.length-1]=="/") {
            filePath = this.serverRoot+request.url+"index.html";
        }
        var fileExtenstion = "."+filePath.split(".").reverse()[0];
        var contentType = IBFoundation.mimeTypes[fileExtenstion] || "application/octet-stream";
        
        FS.readFile(filePath, function (error, content) {
            if (error) {
                if (error.code == "ENOENT") {
                    IBFoundation.serveErrorPage(404, request, response);
                } else if (error.code == "EISDIR") {
                    IBFoundation.serveErrorPage(404, request, response);
                }
                IBFoundation.log(2, "ERR", error);
                return;
            }
            response.writeHead(200, { 'Content-Type': contentType });
            response.write(content, ()=>{
                response.end();
            });
        });
    }

    private respondToPOST ( request: HTTP.IncomingMessage, response: HTTP.ServerResponse ) {
        null;
        SQLi3;
    }

}
// IBBuild::RemoveExport
export class Bark {


}
// IBBuild::RemoveExport
export class Branch {

}
/*
var myTrunk = {
    serverPort: 8080, // The main server port at "example.com:8080"
    defaultRedirect: "sub.example.com" || null,
    barkServers: [
        {
            name: "bark1",
            subdomain: "sub",
            rootDirectory: "/sub/",
            barkServers: [
                {}
            ]
        }
    ],  // Subdomains that do not require their own webserver "sub.example.com:8080"
    branchServers: [] // Subdomains that are really other web applications  "app.example.com:8080" would forward to non-public "app:8081"
}

myTrunk;*/

// IBBuild::End
