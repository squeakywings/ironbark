"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var IBDataImport = require("./IronbarkData");
exports.IBData = IBDataImport.IronbarkData;
// IBBuild::Start
// Core Base of the Application...
// Provides constants and variables and some handlers and config init/setup
var IronbarkFoundation = /** @class */ (function () {
    function IronbarkFoundation() {
    }
    /**
     * A function that serves as a debugging tool for the Ironbark server console.
     * This tries to emulate the default console.log() method.
     * @param level Provides the debug level at which the message is printed.
     * @param message The text to be printed if the level parameter is less than or equal to the debug level.
     *
     * @returns {boolean} Returns whether or not the message was printed.
     */
    IronbarkFoundation.log = function (level, message) {
        var optionalParams = [];
        for (var _i = 2; _i < arguments.length; _i++) {
            optionalParams[_i - 2] = arguments[_i];
        }
        if (level <= this.DEBUG_LEVEL) {
            optionalParams.unshift(message);
            console.log.apply(null, optionalParams);
            return true;
        }
        return false;
    };
    IronbarkFoundation.serveErrorPage = function (code, request, response, onlyHeader) {
        /*function basicError(code:number):string {
            let msg = IronbarkFoundation.errorMessages[code];
            return `<h1 style="font-family: sans-serif; font-size: 3em;">HTTP Error ${String(code)}</h1><hr/><h2 style="font-family: sans-serif; font-size: 2em;">${msg}</h2>`;
        };*/
        if (code === void 0) { code = 500; }
        if (onlyHeader === void 0) { onlyHeader = false; }
        function generateErrorPage(code) {
            var msg = IronbarkFoundation.statusMessages[code];
            return ("<html>\n            <head>\n                <title>Ironbark " + String(code) + "</title>\n                <style>\n                    body {\n                        font-family: 'Segoe UI', sans-serif;\n                        background: #222;\n                        text-align: center;\n                        position: relative;\n                        color: white;\n                    }\n                    h1 {\n                        font-size: 3em;\n                        margin: 1em 0;\n                    }\n                    img {\n                        margin: 1em 0;\n                        width: 15em;\n                        height: 15em;\n                        margin-bottom: -1em;\n                    }\n                </style>\n            </head>\n            <body>\n                <img alt=\"Ironbark Logo\" src=\"" + (exports.IBData.websiteIcon.header + exports.IBData.websiteIcon.data) + "\" />\n                <h1 style=\"font-size: 3em;margin: 1em 0;\">HTTP ERROR " + String(code) + "</h1>\n                <hr style=\"background: white; border: 2px solid white;\" />\n                <p style=\"font-size: 1.5em;\">" + msg + "</p>\n            </body>\n            </html>").split('\n').join("");
        }
        if (onlyHeader) {
            response.writeHead(code);
            //response.write(generateErrorPage(code), ()=>{
            //    response.end();
            //});
            response.end();
            return;
        }
        else {
            response.writeHead(code, { "Content-Type": "text/html" });
            response.write(generateErrorPage(code), function () {
                response.end();
            });
            return;
        }
    };
    IronbarkFoundation.DEBUG_LEVEL = 0; //Default debugging level
    IronbarkFoundation.mimeTypes = {
        //'': 'text/plain',
        '.html': 'text/html',
        '.htm': 'text/html',
        '.js': 'text/javascript',
        '.css': 'text/css',
        '.json': 'application/json',
        '.php': 'text/html',
        '.png': 'image/png',
        '.jpg': 'image/jpeg',
        '.jpeg': 'image/jpeg',
        '.gif': 'image/gif',
        '.wav': 'audio/wav',
        '.mp3': 'audio/mpeg3',
        '.mp4': 'video/mp4',
        '.mov': 'video/quicktime',
        '.woff': 'application/font-woff',
        '.ttf': 'application/font-ttf',
        '.eot': 'application/vnd.ms-fontobject',
        '.otf': 'application/font-otf',
        '.svg': 'application/image/svg+xml',
        '.md': 'text/markdown'
    };
    IronbarkFoundation.statusMessages = {
        400: "Your browser made an error.",
        401: "Your are not authorised to access this page.",
        403: "You are not allowed to access this page.",
        404: "The page you were looking for doesn't exist.",
        500: "Something went wrong on our end."
    };
    return IronbarkFoundation;
}());
exports.IronbarkFoundation = IronbarkFoundation;
// IBBuild::End
