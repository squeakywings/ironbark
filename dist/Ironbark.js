"use strict";
//@ts-check
/*
  The MDN Web Docs were a huge help in learning JS for me:
    This link in particular helped to start me with NodeJS...
    https://developer.mozilla.org/en-US/docs/Learn/Server-side/Node_server_without_framework
*/
Object.defineProperty(exports, "__esModule", { value: true });
var HTTP = require("http"); // NodeJS HTTP Module
var FS = require("fs"); // NodeJS File System Module
var SQLi3 = require("sqlite3"); // NodeJS SQLite3 Module
var IBFoundationImport = require("./IronbarkFoundation");
var IBDataImport = require("./IronbarkData");
var IBModuleLoader = require("./IronbarkModuleLoader");
exports.IBFoundation = IBFoundationImport.IronbarkFoundation;
var IBData = IBDataImport.IronbarkData;
IBModuleLoader;
var Configuration = /** @class */ (function () {
    // Recursively filter through each folder from the root and find a '.ironbark' file for configuration.
    // Every '.ironbark' becomes a 'root' for the path heierachy.
    function Configuration() {
    }
    return Configuration;
}());
/*
IronbarkWebServer
|-bin
|-docs
|-modules
|-ironbark-www
 |-Admin
  |-admin1
  |-sysadmin
 |-Users
  |-2020
   |-LENE08
    |-.ironbark
    |-Data
     |-MyWebsite!
  |-2021
  |-2022

*/
var Trunk = /** @class */ (function () {
    function Trunk(serverPort, serverOptions) {
        var _this = this;
        this.serverPort = 80;
        this.barkServers = [];
        this.serverPort = serverPort;
        this.httpServer = HTTP.createServer(function (req, res) { _this.respondToRequest(req, res); });
        this.serverRoot = "./ironbark-www"; //|| process.execPath;
    }
    Object.defineProperty(Trunk.prototype, "port", {
        get: function () { return this.serverPort; },
        enumerable: true,
        configurable: true
    });
    ;
    Trunk.prototype.start = function () {
        this.httpServer.listen(this.serverPort);
        exports.IBFoundation.log(0, "Started Server on PORT:", this.serverPort);
    };
    Trunk.prototype.stop = function () {
        var _this = this;
        this.httpServer.close(function () {
            exports.IBFoundation.log(0, "Closed Server on PORT:", _this.serverPort);
        });
    };
    Trunk.prototype.test = function () {
        //FS.lstatSyn
    };
    Trunk.prototype.respondToRequest = function (request, response) {
        //IronbarkFoundation.log(2, request.method,">>",request.url);
        exports.IBFoundation.log(2, request.headers);
        switch (request.method) {
            case "HEAD":
                this.respondToHEAD(request, response);
                break;
            case "GET":
                this.respondToGET(request, response);
                break;
            case "POST":
                this.respondToPOST(request, response);
                break;
            default:
                response.writeHead(400, request.method + " request method is not supported.", { "Allow": "GET, POST, HEAD" });
                break;
        }
    };
    Trunk.prototype.respondToHEAD = function (request, response) {
        response.writeHead(200);
        response.end();
    };
    Trunk.prototype.respondToGET = function (request, response) {
        var filePath = "." + request.url;
        var filePath = this.serverRoot + request.url;
        if (filePath[filePath.length - 1] == "/") {
            filePath = this.serverRoot + request.url + "index.html";
        }
        var fileExtenstion = "." + filePath.split(".").reverse()[0];
        var contentType = exports.IBFoundation.mimeTypes[fileExtenstion] || "application/octet-stream";
        FS.readFile(filePath, function (error, content) {
            if (error) {
                if (error.code == "ENOENT") {
                    exports.IBFoundation.serveErrorPage(404, request, response);
                }
                else if (error.code == "EISDIR") {
                    exports.IBFoundation.serveErrorPage(404, request, response);
                }
                exports.IBFoundation.log(2, "ERR", error);
                return;
            }
            response.writeHead(200, { 'Content-Type': contentType });
            response.write(content, function () {
                response.end();
            });
        });
    };
    Trunk.prototype.respondToPOST = function (request, response) {
        null;
        SQLi3;
    };
    return Trunk;
}());
exports.Trunk = Trunk;
var Bark = /** @class */ (function () {
    function Bark() {
    }
    return Bark;
}());
exports.Bark = Bark;
var Branch = /** @class */ (function () {
    function Branch() {
    }
    return Branch;
}());
exports.Branch = Branch;
/*
var myTrunk = {
    serverPort: 8080, // The main server port at "example.com:8080"
    defaultRedirect: "sub.example.com" || null,
    barkServers: [
        {
            name: "bark1",
            subdomain: "sub",
            rootDirectory: "/sub/",
            barkServers: [
                {}
            ]
        }
    ],  // Subdomains that do not require their own webserver "sub.example.com:8080"
    branchServers: [] // Subdomains that are really other web applications  "app.example.com:8080" would forward to non-public "app:8081"
}

myTrunk;*/
// IBBuild::End
