export declare namespace IronbarkData {
    const websiteIcon: {
        "header": string;
        "data": string;
    };
}
