"use strict";
exports.__esModule = true;
var fs = require("fs");
var fileString = "";
var myRegex = /(\/\/ IBBuild::Start)(.*?)(\/\/ IBBuild::End)/gms;
//var myRegex = /(?<=\/\/ IBBuild::Start\n)(.*?)(?=\/\/ IBBuild::End)/gms
var myArray;
/*
// IBBuild::Enable  - Include anything under but not above.
// IBBuild::Start   - Include from here...
// IBBuild::End     - To here.
 */
console.log("BEGINNING BUILD");
var MODULES_PATH = "./src/";
var modulesToLoad = fs.readdirSync(MODULES_PATH);
var concatFile = [];
var success = false;
for (var _i = 0, modulesToLoad_1 = modulesToLoad; _i < modulesToLoad_1.length; _i++) {
    var filename = modulesToLoad_1[_i];
    var name = filename.split(".").splice(0, filename.split(".").length - 1).join(".");
    var extension = filename.split(".")[filename.split(".").length - 1];
    if (extension == "ts" && name != "IronbarkBuild") {
        var includeFilename = true;
        console.log("Found : " + name + "." + extension);
        fileString = fs.readFileSync(MODULES_PATH + name + "." + extension).toString();
        //console.log(fileString);
        fileString = fileString.replace(/(\r\n|\r\n\t|\n|\r\t|\r)/gm, "\n");
        fileString = fileString.replace(/(\/\/ IBBuild::RemoveExport)(.*?)(export)( )*/gms, "");
        //console.log(/(\/\/ IBBuild::RemoveExport)(.*?)(export)/gms.exec(fileString));
        success = false;
        while ((myArray = myRegex.exec(fileString)) !== null) {
            success = true;
            if (includeFilename) {
                concatFile.push("//\n// IBBuildModule::" + name + "\n//");
                includeFilename = false;
            }
            //console.log(myArray[0]);
            var content = myArray[0];
            //content = content.replace(/(\r\n|\r\n\t|\n|\r\t|\r)/gm,"\n");
            content = content.replace(/(\/\/ IBBuild::Start|\/\/ IBBuild::End)/gms, "");
            //console.log(content);
            //console.log(/\/\/ IBBuild::RemoveExport.export/gms.exec(content));
            //console.log(content);
            concatFile.push(content);
        }
        if (!success && /\/\/( |)IBBuild::Enable/gms.test(fileString)) {
            if (includeFilename) {
                concatFile.push("//\n// IBBuildModule::" + name + "\n//");
                includeFilename = false;
            }
            concatFile.push(fileString);
        }
    }
}
fs.writeFileSync("./build/bin/IronbarkServer.ts", concatFile.join("\n"));
